# Ornikar TEST

## Part I : Make an analysis of the insurance Ornikar site (<https://www.ornikar.com/assurance-auto>)

1. In a few words describe the website and explain the purpose of the company.

The web page, when it opens, explains the status of the offer and how to be kept informed of its evolution.
When you scroll down the web page, you will notice that it is divided into several parts, to answer questions (why, how, what, etc.).

Ornikar is an online driving school that has historically assisted candidates in revising their driving code and booking driving lessons.
For a few years now, it has been offering a new insurance package for young drivers who have been trained on the platform and then for any other driver who wishes to benefit from it.

2. Explain what you will test first and why.

For best practice, I would advise pyramid testing, i.e. start with static tests, then unit tests, then integration tests and finally interface tests.
The static tests will allow to find typing errors, syntax, ...
The unit tests in second, will make it possible to validate that the behavior and the critical functionalities of the system.
These two tests will allow bugs to be identified very early on before the integration tests and interface tests are run.

3. Imagine you are with your three amigos, how do you explain to them what we have to test.

Static and unit tests will allow bugs and errors to be detected and corrected as the development process progresses. Because testing and fixing early on reduces costs and time.

4. Write a test strategy of your choice and explain it.

See word document.

5. What kind of framework automation do you like and explain why you want to use it (pros / con)

To carry out test automation, I recommend the use of the Cypress Framework.

Advantages :
 - The interface is easy to use (simple and intuitive).
 - It can be used for unit testing, integration testing and end-to-end testing.
 - The tests can be run in parallel so they run quickly.
 - It can be used to simulate different browsers.

Disadvantages:
 - It does not support the Safari browser.
 - It is only available for JavaScript projects.

6. Explain your automation process for the UI
 a. Read the specification documents.
 b. Develop the test cases in Gerkhin language and have them validated.
 c. Automate the test cases
 d. Validating the automated tests.

7. Send us an example that you use of QA report
	- See excel file

## Part II : Make an analysis of the API (<http://dummy.restapiexample.com/>)

1. Explain to the new product owner what are the api’s for and their purpose

An API is a mechanism for one application or service to access another application or service. The application or service making the access is called the client and the application or service containing the resource is called the server.  
REST APIs communicate via HTTP requests to perform standard database functions such as creating, reading, updating and deleting records (also known as CRUDs) in a resource. For example, a REST API would use a GET request to retrieve a record, a POST request to create a record, a PUT request to update a record and a DELETE request to delete a record. All HTTP methods can be used in API calls.

2. Write a test strategy
	-See word file

3. What kind of framework automation do you like and explain why you want to use it (pros / con)

Advantages :  
 - It has advanced support for web APIs, which means you can test, run and develop them.
 - It is easy to use with an intuitive and integrated interface.
 - It is easy to configure and set up.
 - Flexible scripting engine that allows you to automate tests.
 - Provides a variety of other useful tools for performance analysis and data capture.

Disadvantages :  
 - Price may be an issue for some users.
 - There are not many resources to help them.

4. Explain your automation process for the API  
 a. Read the specification documents.  
 b.  Manually testing the API  
 c. Undertand the response returned by the API (smoke test)  
 d. Write the automated test  
    1. Before you make a request.  
   These scripts are called pre-request script and you can write them under the Pre-request Script tab.  
    2. After you’ve received a response from the request you made.  
      	- These scripts are called Test scripts and it is this set of scripts.  

 e. Run test  
 f. Validating the automated tests.  

1. Send us an example that you use for a QA report.
 See excel file
